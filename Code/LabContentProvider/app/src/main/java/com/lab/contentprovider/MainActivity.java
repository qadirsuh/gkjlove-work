package com.lab.contentprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.ListActivity;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


/*Step 4: Extend your MainActivity with ListActivity and declare a request code. So your class declaration should look like:*/

public class MainActivity extends ListActivity {

    final private int MY_PERMISSION_REQUEST_READ_CONTACTS = 567;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Step 5: Check for permission by appending the following code inside onCreate()
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSION_REQUEST_READ_CONTACTS);
        } else {
            ListContacts();
        }

    }

    // Step 6: Define ListContacts() as:
    protected void ListContacts() {
        Uri allContacts = Uri.parse("content://contacts/people");

        Cursor c;
        CursorLoader cursorLoader = new CursorLoader(this, allContacts, null, null, null, null);
        c = cursorLoader.loadInBackground();

        String[] columns = new String[]{
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts._ID};
        int[] views = new int[]{R.id.contactName, R.id.contactID};

        SimpleCursorAdapter adapter =
                new SimpleCursorAdapter(this, R.layout.activity_main, c, columns, views, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        // ListViewmyList = (ListView)findViewById(android.R.id.list);
        //myList.setAdapter(adapter);
        this.setListAdapter(adapter);
    }

    // Step 7: Deal with permission result by defining onRequestPermissionsResult() as follows:
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_READ_CONTACTS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ListContacts();
                } else {
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
