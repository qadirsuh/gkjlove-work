package com.lab.service2;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    // Step 12.4 Add the following code to main activity
    MyService serviceBinder;
    Intent i;
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(
                ComponentName className, IBinder service) {
            serviceBinder = ((MyService.MyBinder) service).getService();
            try {
                URL[] urls = new URL[]
                        {
                                new URL("http://www.goole.com/photo1.png"),
                                new URL("http://www.goole.com/photo1.png"),
                                new URL("http://www.goole.com/photo1.png"),
                                new URL("http://www.goole.com/photo1.png")
                        };
                serviceBinder.urls = urls;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            startService(i);  // start the bound service
        }

        public void onServiceDisconnected(ComponentName className) {
            serviceBinder = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Step 3.5 add startService code to main activity
    public void startService(View view) {

        // startService(new Intent(getBaseContext(), MyService.class));

        // Step 4: Change your startService to invoke MyIntentService.class
        // startService(new Intent(getBaseContext(), MyIntentService.class));

        // simulate passing file name from activity for download.
        // Step 9: In the main activity, let’s switch back to MyService.class by coding the startService as the following:

        /*Intent intent = new Intent(getBaseContext(), MyService.class);
        try {
            // pack the file names into array
            URL[] urls = new URL[]{
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png")};
            intent.putExtra("URLs", urls);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        startService(intent);*/

        // Step 12.5 Modify startService(View view) as follows:

        i = new Intent(MainActivity.this, MyService.class);
        bindService(i, connection, Context.BIND_AUTO_CREATE);


    }

    // Step 5.2 add stopService code to main activity
    public void stopService(View view) {
        // stopService(new Intent(getBaseContext(), MyService.class));

        // Step 4: Change your stopService to invoke MyIntentService.class
        stopService(new Intent(getBaseContext(), MyIntentService.class));
    }

    // Step 7: Put the code in the main activity (at the beginning is fine)
    IntentFilter intentFilter;

    @Override
    public void onResume() {
        super.onResume();
        intentFilter = new IntentFilter();
        intentFilter.addAction("FILE_DOWNLOADED_ACTION");
        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getBaseContext(), "File downloaded", Toast.LENGTH_LONG).show();
        }
    };

}
