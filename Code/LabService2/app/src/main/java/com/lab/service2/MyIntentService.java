package com.lab.service2;


// Of course, if you key in your code right, your header should have the following libraries:

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */

//  Continue the app, Simple Service, created in Lab Android Services I
//  Step 1:	Add a new Java class, MyIntentService.java with Superclass IntentService (when you type in IntentService the IDE will prompt you to select android.app.IntentService) then click OK button. In the java file, click the red light bulb and select “Implement methods” (but still have an error for no default constructor).
//  Step 2: Populate the class with code so that your class will look like the following:

public class MyIntentService extends IntentService {

    private Thread thread = new Thread();
    String TAG = "MyIntentService";

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        thread.start();
        try {
            int result = DownloadFile(new URL("http://www.google.com/photo.png"));
            Log.d(TAG, "Downloaded: " + result + " bytes");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Step 6: Prepare and send broadcast. Append the code in the try block inside onHandleIntent()
        // prepare and send broadcast

        try {

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("FILE_DOWNLOADED_ACTION");
            getBaseContext().sendBroadcast(broadcastIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int DownloadFile(URL url) {
        try {
            thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 500;
    }
}
