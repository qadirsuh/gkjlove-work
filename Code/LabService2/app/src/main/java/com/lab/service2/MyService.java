package com.lab.service2;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;

//Step 2: Create a service
//Step 2.1 Add a new Java Service Class file (right click java > New > Service) and name it MyService (with Superclass be Service for android.app.Service)

public class MyService extends Service {
    public MyService() {
    }


    // A better approach is to bind activity directly to service. So that activity can call any public members and methods on the service directly.
    // Step 12: Bind activities to services locally
    // Step 12.1: Add the following statements to MyService.java

    int counter = 0;
    URL[] urls;
    static final int UPDATE_INTERVAL = 1000;
    private Timer timer = new Timer();

    public class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    private final IBinder binder = new MyBinder();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();

        // Step 7: Simulate a long running task
        // Modify onStartCommand() and add a new method DownloadFile() to simulate a lengthy task in MyService.java

        /*try {
            int result = DownloadFile(new URL("http://www.google.com/myPhotos.png"));
            Toast.makeText(getBaseContext(), "Downloaded " + result + " bytes", Toast.LENGTH_LONG).show();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/


        // Step 10: Modify the onStartCommand to execute the inner class by commenting try and catch clause
        /*try {
            new DoBackgroundTask().execute(
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/

        // Step 10: Modify onStartCommand () in MyService.java as the following:

        // unpack file names
        Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();

        // Step 12.3 Modify onStartCommand()
        // Comment out the block (no need to unpack the files)


        /*Object[] objUrls = (Object[]) intent.getExtras().get("URLs");
        URL[] urls = new URL[objUrls.length];
        for (int i = 0; i < objUrls.length - 1; i++) {
            urls[i] = (URL) objUrls[i];
        }*/
        new DoBackgroundTask().execute(urls);

        return START_STICKY;
    }

    private int DownloadFile(URL url) {
        try {
            Thread.sleep(10000);  //10 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 500;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service destroyed", Toast.LENGTH_LONG).show();
    }


    @Override
    public IBinder onBind(Intent intent) {

        // Step 12.2 Modify onBind() to be:
        return binder;
    }


    // Step 8: Test the app to see the app will be tied up for 10 seconds before Toast. That is why need to run service in a separate thread.
    // Step 9: Use AsyncTask for easy implementation (Create AsyncTask for Service task)
    // Step 9.1 Modify MyService.java to create an AsyncTask subclass containing the code from onStartCommand() method:

    // inner class
    private class DoBackgroundTask extends AsyncTask<URL, Integer, Long> {

        @Override
        // On background thread
        protected Long doInBackground(URL... params) {
            int count = params.length;
            long totalBytesDownloaded = 0;
            for (int i = 0; i < count; i++) {
                totalBytesDownloaded += DownloadFile(params[i]);
                // pass result to onPregressUpdate to report progress
                publishProgress((int) (((i + 1) / (float) count) * 100));
            }
            return totalBytesDownloaded;
        }

        // On UI thread
        protected void onProgressUpdate(Integer... progress) {
            Log.d("Downloading: ", String.valueOf(progress[0]) + "% downloaded");
            Toast.makeText(getBaseContext(), String.valueOf(progress[0]) + "% downloaded",
                    Toast.LENGTH_LONG).show();
        }

        // On UI thread
        protected void onPostExecute(Long result) {
            Toast.makeText(getBaseContext(), "Download: " + result + " bytes",
                    Toast.LENGTH_LONG).show();
            stopSelf();
        }
    }
}
