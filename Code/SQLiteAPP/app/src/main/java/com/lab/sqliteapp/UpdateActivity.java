package com.lab.sqliteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

// Step 35: Using empty activity template without creating the layout xml file (as what we did for DeleteActivity mentioned above) to create a new activity, UpdateActivity.

public class UpdateActivity extends AppCompatActivity {

    // Step 38.1: Create an instance variable
    DatabaseManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Step 38.3: Finish the onCreate() to be
        super.onCreate(savedInstanceState);
        dbManager = new DatabaseManager(this);
        updateView();

    }

    // Step 38.2: Create an empty method updateView()
    // Step 38.4: Code for updateView()
    public void updateView() {
        ArrayList<Candy> candies = dbManager.selectAll();
        if (candies.size() > 0) {
            // create ScrollView and GridLayout
            ScrollView scrollView = new ScrollView(this);
            GridLayout grid = new GridLayout(this);
            grid.setRowCount(candies.size());
            grid.setColumnCount(4);

            // Create arrays to store data
            TextView[] ids = new TextView[candies.size()];
            EditText[][] namesAndPrices = new EditText[candies.size()][2];
            Button[] buttons = new Button[candies.size()];
            ButtonHandler bh = new ButtonHandler(); // we will define the handler later on

            // obtain width of screen
            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int width = size.x;

            int i = 0;

            for (Candy candy : candies) {
                // populate ids
                ids[i] = new TextView(this);
                ids[i].setGravity(Gravity.CENTER);
                ids[i].setText("" + candy.getId());

                // populate name sand prices
                namesAndPrices[i][0] = new EditText(this);
                namesAndPrices[i][1] = new EditText(this);
                namesAndPrices[i][0].setText(candy.getName());
                namesAndPrices[i][1].setText("" + candy.getPrice());
                namesAndPrices[i][1].setInputType(InputType.TYPE_CLASS_NUMBER);
                namesAndPrices[i][0].setId(10 * candy.getId());
                namesAndPrices[i][1].setId(10 * candy.getId() + 1);

                // populate update button
                buttons[i] = new Button(this);
                buttons[i].setText("Update");
                buttons[i].setId(candy.getId());

                buttons[i].setOnClickListener(bh);

                // add elements to grid
                grid.addView(ids[i], width / 10, ViewGroup.LayoutParams.WRAP_CONTENT);
                grid.addView(namesAndPrices[i][0], (int) (width * .4), ViewGroup.LayoutParams.WRAP_CONTENT);
                grid.addView(namesAndPrices[i][1], (int) (width * .15), ViewGroup.LayoutParams.WRAP_CONTENT);
                grid.addView(buttons[i], (int) (width * .35), ViewGroup.LayoutParams.WRAP_CONTENT);

                i++;
            }
            scrollView.addView(grid);
            setContentView(scrollView);
        }
    }

    // Step 38.5: Define button handler as:
    private class ButtonHandler implements View.OnClickListener {
        public void onClick(View v) {
            // retrieve name and price
            int candyId = v.getId();
            EditText nameET = (EditText) findViewById(10 * candyId);
            EditText priceET = (EditText) findViewById(10 * candyId + 1);
            String name = nameET.getText().toString();
            String priceString = priceET.getText().toString();

            // update candy in database
            try {
                double price = Double.parseDouble(priceString);
                dbManager.updateById(candyId, name, price);
                Toast.makeText(UpdateActivity.this, "Candy updated", Toast.LENGTH_SHORT).show();

                // refresh screen
                updateView();
            } catch (NumberFormatException nfe) {
                Toast.makeText(UpdateActivity.this, "Price Error", Toast.LENGTH_LONG).show();
            }
        }
    }
}
