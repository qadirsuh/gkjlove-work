package com.lab.sqliteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.ArrayList;

// Step 33.1: Create the third activity, DeleteActivity
// Right click project, New | Activity | Empty Activity (Make sure you change the default name for the activity to DeleteActivity) and Uncheck “Generate Layout File” and click Finish button (we need to build the GUI dynamically because we don’t know the number of records.)

// Since we will build each record as a radio button, we need to implement interface for onCheckedChangeListener.

public class DeleteActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    // Step 33.4: Declare an instance variable for DatbaseManager (in DeleteActivity.java)
    private DatabaseManager dbManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbManager = new DatabaseManager(this);
        updateView();


    }

    // Step 33.5: Create a method to build view dynamically
    public void updateView() {

        //Step 33.8: Finish updateView() as follows:
        // Build a view dynamically
        ArrayList<Candy> candies = dbManager.selectAll();
        RelativeLayout layout = new RelativeLayout(this);
        ScrollView scrollView = new ScrollView(this);
        RadioGroup group = new RadioGroup(this);
        // populate the radio group
        for (Candy candy : candies) {
            RadioButton rb = new RadioButton(this);
            rb.setId(candy.getId()); // this is the tricky part to learn
            rb.setText(candy.toString());
            group.addView(rb);  // need to add each radio button to the group
        }
        // set up event handler for the radio button group
        RadioButtonHandler rbh = new RadioButtonHandler();
        group.setOnCheckedChangeListener(rbh);

        // Provide a back button for user
        // not needed for this button if you want user to use the device back button
        // Notice the back button is attached to the relative layout and not the scroll view
        // because scroll view does not allow more than one view group in it

        Button backButton = new Button(this);
        backButton.setText(R.string.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeleteActivity.this.finish(); // pay attention to this statement
            }
        });

        // add radio group to scroll view
        scrollView.addView(group);
        // add scrollview to the relative layout
        layout.addView(scrollView);

        // add back button to the relative layout
        RelativeLayout.LayoutParams params
                = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.setMargins(0, 0, 0, 60);
        layout.addView(backButton, params);

        setContentView(layout);  // link the layout to the activity

    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        dbManager.deleteById(checkedId);
        Toast.makeText(DeleteActivity.this, "Candy deleted", Toast.LENGTH_SHORT).show();

        //refresh the screen
        updateView();
    }

    // Step 33.7: Implement a class to handle RadioGroup.OnCheckedChangeListener in DeleteActivity
    // Make sure when studio prompts you for multiple choices you select the right on by clicking alt enter
    private class RadioButtonHandler
            implements RadioGroup.OnCheckedChangeListener {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // delete the selected record from database
            dbManager.deleteById(checkedId);
            Toast.makeText(DeleteActivity.this, "Candy deleted", Toast.LENGTH_SHORT).show();

            //refresh the screen
            updateView();
        }
    }
}

