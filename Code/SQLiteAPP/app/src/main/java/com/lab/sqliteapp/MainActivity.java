package com.lab.sqliteapp;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Step 46: About time to work on MainActivity
    // Step 46.1: Declare 4 instance variables

    private DatabaseManager dbManager;
    private double total;
    private ScrollView scrollView;
    private int buttonWidth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Step 5: Remove FloatingActionButton code block inside onCreate() in MainActivity.java

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        // Step 46.3: Append the following statements into onCreate()

        dbManager = new DatabaseManager(this);
        total = 0.0;
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        buttonWidth = size.x / 2;
        updateView();

    }

    // Step 46.2: Define am empty updateView() method
    public void updateView() {

        // Step 46.7: Finish updateView() as follows:
        ArrayList<Candy> candies = dbManager.selectAll();
        if (candies.size() > 0) {
            scrollView.removeAllViewsInLayout();  // make sure acroll view is fresh

            GridLayout grid = new GridLayout(this);
            grid.setRowCount((candies.size() + 1 / 2));
            grid.setColumnCount(2);

            CandyButton[] buttons = new CandyButton[candies.size()];
            ButtonHandler bh = new ButtonHandler();

            // populate the grid
            int i = 0;
            for (Candy candy : candies) {
                buttons[i] = new CandyButton(this, candy);
                buttons[i].setText(candy.getName() + "\n" + candy.getPrice());
                buttons[i].setOnClickListener(bh);

                grid.addView(buttons[i], buttonWidth, GridLayout.LayoutParams.WRAP_CONTENT);
                i++;
            }
            scrollView.addView(grid);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // Step 16: Add code to onOptionsItemSelected() to prompt user when a menu item is selected

        int id = item.getItemId();

        switch (id) {
            case R.id.action_add:
                Log.w("Main Activity", "Add selected");
                // Use Toast if Log is not working
                //Toast.makeText(getApplicationContext(), "ADD", Toast.LENGTH_LONG).show();

                // Step 20: Change case for action_add in MainActivity to be:
                //Log.w("Main Activity", "Add selected");
                //Toast.makeText(getApplicationContext(), "ADD", Toast.LENGTH_LONG).show();
                Intent insertIntent = new Intent(this, InsertActivity.class);
                this.startActivity(insertIntent);
                return true;

            // Step 33.2: Modify the case R.id.action_delete inside switch statement  inMainActivity as:
            case R.id.action_delete:

                //Log.w("Main Activity", "Delete selected");
                Intent deleteIntent = new Intent(this, DeleteActivity.class);
                this.startActivity(deleteIntent);
                return true;

            // Step 37: Go to MainActivity and change the case R.id.action_update accordingly
            case R.id.action_update:
                //Log.w("Main Activity", "Update selected");
                Intent updateIntent = new Intent(this, UpdateActivity.class);
                this.startActivity(updateIntent);
                return true;

            // Step 46.4: Add the action in onOptionsItemSelected() for RESET menu item

            case R.id.action_reset:
                total = 0.0;
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


        // noinspection SimplifiableIfStatement
        // Step 13: Remove “if” statement in onOptionsItemSelected() in MainActivity.java

        /*if (id == R.id.action_settings) {
            return true;
        }*/
    }

    // Step 46.5: Define listener for each candy
    private class ButtonHandler implements View.OnClickListener {
        public void onClick(View v) {
            total += ((CandyButton) v).getPrice();
            String pay = NumberFormat.getCurrencyInstance().format(total);
            Toast.makeText(MainActivity.this, pay, Toast.LENGTH_LONG).show();
        }
    }

    // Step 46.6: Define onResume()
    protected void onResume() {
        super.onResume();
        updateView();
    }

}
