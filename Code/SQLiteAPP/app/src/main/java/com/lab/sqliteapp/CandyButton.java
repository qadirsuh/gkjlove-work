package com.lab.sqliteapp;

import android.content.Context;

import androidx.appcompat.widget.AppCompatButton;

// Step 45: Define CandyButton as follows:
public class CandyButton extends AppCompatButton {

    private Candy candy;

    public CandyButton(Context context, Candy newCandy) {
        super(context);
        candy = newCandy;
    }

    public double getPrice() {
        return candy.getPrice();
    }
}