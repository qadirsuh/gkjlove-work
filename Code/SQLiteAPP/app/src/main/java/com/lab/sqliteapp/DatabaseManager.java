package com.lab.sqliteapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

// Step 28: Create a subclass of SQLiteOpenHelper
// Right click project, New | Java Class
// In the Create New Class dialog:
// Name: DatabaseManager


public class DatabaseManager extends SQLiteOpenHelper {

    // Step 30.1: Define the following variables
    private static final String DATABASE_NAME = "candyDB.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CANDY = "candy";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PRICE = "price";

    // sStep 30.2: Create a constructor:
    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Step 30.3: Finish auto-created onCreate()
        String sqlCreate = "create table " + TABLE_CANDY + "(" + ID;
        sqlCreate += " integer primary key autoincrement, " + NAME;
        sqlCreate += " text, " + PRICE + " REAL)";

        Log.e("sqlCreate", sqlCreate);

        db.execSQL(sqlCreate);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Step 30.4: Finish auto-created onUpgrade()

        // these statements are for demo only
        // in real application you may use alter
        db.execSQL("drop table if exists " + TABLE_CANDY);
        onCreate(db);

    }

    // Step 30.5: insert a record
    public void insert(Candy candy) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlInsert = "insert into " + TABLE_CANDY + "(" + NAME + ", " + PRICE + ")";

        sqlInsert += " values('" + candy.getName() + "',";
        sqlInsert += "'" + candy.getPrice() + "')";

//        sqlInsert += " values ('" + candy.getName() + "',";
//        sqlInsert += candy.getPrice() + ")";

        db.execSQL(sqlInsert);
        db.close();
    }

    // Step 30.6: delete a record
    public void deleteById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlDelete = "delete from " + TABLE_CANDY;
        sqlDelete += " where " + ID + " = " + id;

        db.execSQL(sqlDelete);
        db.close();
    }

    // Step 30.7: update a record
    public void updateById(int id, String name, double price) {
        SQLiteDatabase db = this.getWritableDatabase();

        String sqlUpdate = "update " + TABLE_CANDY;
        sqlUpdate += " set " + NAME + " = '" + name + "', ";
        sqlUpdate += PRICE + " = '" + price + "'";
        sqlUpdate += " where " + ID + " = " + id;

        db.execSQL(sqlUpdate);
        db.close();
    }

    // Step 30.8: retrieve all records
    public ArrayList<Candy> selectAll() {
        String sqlQuery = "select * from " + TABLE_CANDY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);

        ArrayList<Candy> candies = new ArrayList<Candy>();
        while (cursor.moveToNext()) {
            Candy currentCandy
                    = new Candy(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getDouble(2));
            candies.add(currentCandy);
        }
        db.close();
        return candies;
    }

    // Step 30.9: retrieve a specific record
    public Candy selectById(int id) {
        String sqlQuery = "select * from " + TABLE_CANDY;
        sqlQuery += " where " + ID + " = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);

        Candy candy = null;
        if (cursor.moveToFirst())
            candy = new Candy(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getDouble(2));
        return candy;
    }




}
