package com.lab.sqliteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

// Step 17: Create the second activity, InsertActivity
// Right click project, New | Activity | Empty Activity (Make sure you change the default name for the activity to InsertActivity)

public class InsertActivity extends AppCompatActivity {

    // Step 32: Patch the following code to InsertActivity.java
    // Step 32.1: Declare instance variable
    private DatabaseManager dbManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Step 32.2: Instantiate database in onCreate()  (Make sure methodonCreate looks like the following:)
        super.onCreate(savedInstanceState);
        dbManager = new DatabaseManager(this);  // instantiate database
        setContentView(R.layout.activity_insert);

    }

    // Step 21: Prepare insert and goBack in InsertActivity
    public void insert(View v) {
        EditText nameEditText = (EditText) findViewById(R.id.input_name);
        EditText priceEditText = (EditText) findViewById(R.id.input_price);
        String name = nameEditText.getText().toString();
        String priceString = priceEditText.getText().toString();

        // insert into database

        // Step 32.3: Finish the left out “insert into database” block (put the following code below the comment // insert into database)

        try {
            double price = Double.parseDouble(priceString);
            Candy candy = new Candy(0, name, price);
            dbManager.insert(candy);
            Toast.makeText(this, "Candy added", Toast.LENGTH_SHORT).show();
        } catch (NumberFormatException nfe) {
            Toast.makeText(this, "Price error", Toast.LENGTH_SHORT).show();
        }


        // clear data
        nameEditText.setText("");
        priceEditText.setText("");
    }

    public void goBack(View view) {
        this.finish();
    }


}
