package com.lab.mapservice;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


// Step 1:	Create a project, Map Service, by using Google Maps Activity template
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    //Step 21.2: Define instance variables for your main class (where you define private GoogleMap mMap;)

    final private int REQUEST_COURSE_ACCESS = 101;
    boolean permissionGranted = false;
    LocationManager lm;
    LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {


        /*mMap = googleMap;
        // Add a marker in Sydney and move the camera
        *//*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*//*

        // City of Chicago: Latitude: 41.8781  Longitude: -87.6298
        // Replace the location Sydney by Chicago and zoom in to City level (10)

        LatLng chicago = new LatLng(41.8781, -87.6298);   //Negative for W (west) or S (south)
        mMap.addMarker(new MarkerOptions().position(chicago).title("Chicago, Illinois"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(chicago));

        // Then add the following statement:
        // Step 17: You can comment out the zoomTo statement for simplicity.
        // mMap.animateCamera(CameraUpdateFactory.zoomTo(10));  // 10 for City 15 for Streets


        // Step 9: Put the following statements at the end of onMapReady() to change the type of map
        // Other supported types include: MAP_TYPE_NORMAL,// MAP_TYPE_TERRAIN, MAP_TYPE_HYBRID and MAP_TYPE_NONE

        // Step 11: Comment out the line that set the type//mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Step 12: Add zoom control to the fragment
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Step 18: To get the latitude and longitude of a point that was touched
        //Put the following code in onMapReady after the statement mMap = googleMap; or after mMap.moveCamera(….) That is,

        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {

                *//*Toast.makeText(getApplicationContext(), "onClick " + point.latitude + "/" + point.longitude, Toast.LENGTH_LONG).show();
                Log.d("Map Service", "Map clicked [" + point.latitude + " / " + point.longitude + " ] ");*//*

                // Step 20: Using geocoding to find the location for a latitude and longitude of a location
                // Comment out all existing code in onMapClick() and put the following code in:

                Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder.getFromLocation(point.latitude, point.longitude, 1);
                    String add = "";
                    if (addresses.size() > 0) {
                        for (int i = 0; i <= addresses.get(0).getMaxAddressLineIndex(); i++)
                            add += addresses.get(0).getAddressLine(i) + "\n";
                    }
                    Toast.makeText(getBaseContext(), add, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Notes: With the address of a location you can use GeoCoding to find its latitude and longitude as:
                *//*Geocoder geoCoder1 = new Geocoder(getBaseContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geoCoder1.getFromLocationName("White House", 3);
                    if (addresses.size() > 0) {
                        LatLng p = new LatLng(
                                (int) (addresses.get(0).getLatitude()),
                                (int) (addresses.get(0).getLongitude()));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(p));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*


            }
        });*/

        mMap = googleMap;
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COURSE_ACCESS);
            return;
        } else {
            permissionGranted = true;
        }
        // if you put 0 , 0 here you may face memory issue update too often
        if (permissionGranted) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);
        }


    }

    // Step 14: You can also write code to provide zooming services. But make sure your emulator accepts keyboard input (You can modify it through AVD manager)
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_2:
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                break;
            case KeyEvent.KEYCODE_5:
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
                break;
        }
        return super.onKeyDown(keyCode, event);
    }


    // Step 21: Now let’s use GPS receiver to find your location  (the other two methods are: cell tower triangulation and wi-fi triangulation)
    // Step 21.1: Implement LocationListener. You have to implement onStatusChanged(),  onProviderEnabled() and onProviderDisabled()
    private class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location loc) {
            if (loc != null) {
                Toast.makeText(getBaseContext(), "New Location: Lat-" + loc.getLatitude() + " Lng-" + loc.getLongitude(), Toast.LENGTH_SHORT).show();
                LatLng p = new LatLng((int) (loc.getLatitude()), (int) (loc.getLongitude()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(p));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(5));
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    }

    //Step 21.4: Code for a new method onRequestPermissionResult()
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COURSE_ACCESS:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        permissionGranted = true;
                    } else {
                        permissionGranted = false;
                    }
                    break;
                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    // Step 21.5 Implement onPause() to save the power by removing the lisetner
    @Override
    protected void onPause() {
        super.onPause();

        // remove location listener
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COURSE_ACCESS);
            return;
        } else {
            permissionGranted = true;
        }
        if (permissionGranted) {
            lm.removeUpdates(locationListener);
        }
    }

}
