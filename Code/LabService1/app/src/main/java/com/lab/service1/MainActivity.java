package com.lab.service1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Step 3.5 add startService code to main activity
    public void startService(View view) {
        startService(new Intent(getBaseContext(), MyService.class));
    }

    // Step 5.2 add stopService code to main activity
    public void stopService(View view){
        stopService(new Intent(getBaseContext(), MyService.class));
    }


}
