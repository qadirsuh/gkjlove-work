package com.lab.service1;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

//Step 2: Create a service
//Step 2.1 Add a new Java Service Class file (right click java > New > Service) and name it MyService (with Superclass be Service for android.app.Service)

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();

        // Step 7: Simulate a long running task
        // Modify onStartCommand() and add a new method DownloadFile() to simulate a lengthy task in MyService.java

        /*try {
            int result = DownloadFile(new URL("http://www.google.com/myPhotos.png"));
            Toast.makeText(getBaseContext(), "Downloaded " + result + " bytes", Toast.LENGTH_LONG).show();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/


//        Step 10: Modify the onStartCommand to execute the inner class by commenting try and catch clause
        try {
            new DoBackgroundTask().execute(
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"),
                    new URL("http://www.google.com/photo1.png"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    private int DownloadFile(URL url) {
        try {
            Thread.sleep(10000);  //10 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 500;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service destroyed", Toast.LENGTH_LONG).show();
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    //    Step 8: Test the app to see the app will be tied up for 10 seconds before Toast. That is why need to run service in a separate thread.
//            Step 9: Use AsyncTask for easy implementation (Create AsyncTask for Service task)
//    Step 9.1 Modify MyService.java to create an AsyncTask subclass containing the code from onStartCommand() method:
//
    // inner class
    private class DoBackgroundTask extends AsyncTask<URL, Integer, Long> {

        @Override
        // On background thread
        protected Long doInBackground(URL... params) {
            int count = params.length;
            long totalBytesDownloaded = 0;
            for (int i = 0; i < count; i++) {
                totalBytesDownloaded += DownloadFile(params[i]);
                // pass result to onPregressUpdate to report progress
                publishProgress((int) (((i + 1) / (float) count) * 100));
            }
            return totalBytesDownloaded;
        }

        // On UI thread
        protected void onProgressUpdate(Integer... progress) {
            Log.d("Downloading: ", String.valueOf(progress[0]) + "% downloaded");
            Toast.makeText(getBaseContext(), String.valueOf(progress[0]) + "% downloaded",
                    Toast.LENGTH_LONG).show();
        }

        // On UI thread
        protected void onPostExecute(Long result) {
            Toast.makeText(getBaseContext(), "Download: " + result + " bytes",
                    Toast.LENGTH_LONG).show();
            stopSelf();
        }
    }
}
