package com.lab.fusedlocationprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    // Step 5: Declare variables in java file
    private static final String GooglePlayMSG = "Google Play Services are not available";
    private TextView tv;

    // Step 10: Define permission request code variable
    private static final int LOCATION_PERMISSION_REQUEST = 101;

    // Step 15: Define a location request variable
    private LocationRequest myLocationRequest;

    // Step 17: Define variable
    private boolean mRequestingLocationUpdates = false;


    // challenge code
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Step 6: Append the following statements inside onCreate()
        tv = findViewById(R.id.tv);

        GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
        int result = availability.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (!availability.isUserResolvableError(result)) {
                Toast.makeText(this, GooglePlayMSG, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Get Connected", Toast.LENGTH_LONG).show();
        }


        // Step 16: Define request to the end of onCreate()
        myLocationRequest = new LocationRequest().setInterval(6000).setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    // Step 11: Define onStart() as follows to request for runtime permission
    protected void onStart() {
        super.onStart();
        int permission = ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION);
        if (permission == PERMISSION_GRANTED) {
            getLastLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST);
        }

        // Step 18: Add the statement to be the last statement in onStart()
        mRequestingLocationUpdates = true;
    }


    // Step 8: Implement updateTextView(Location) as follows:
    private void updateTextView(Location location) {
        String latLngString = "No location found";
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            latLngString = "Lat: " + lat + "\nLong: " + lng;
        }
        tv.setText(latLngString);
    }

    //Step 9: Implement getLastLocation() as follows:
    private void getLastLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED || (ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED)) {
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    updateTextView(location);
                }
            });
        }
    }

    // Step 12: Define onRequestPermissionsResult() as follows:
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults[0] != PERMISSION_GRANTED)
                Toast.makeText(this, "Location Permission Denied", Toast.LENGTH_LONG).show();
            else getLastLocation();
        }
    }


    // Step 14: Define a callback to update text view
    LocationCallback myLocationCallBack = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location location = locationResult.getLastLocation();
            if (location != null) {
                updateTextView(location);
            }
        }
    };


    // Step 19: Define startLocationUpdates() as follows:
    private void startLocationUpdates() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED || (ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED)) {
            fusedLocationClient.requestLocationUpdates(myLocationRequest, myLocationCallBack, null /* Looper */);
        }
    }

    // Step 20: Define onResume() as follows:
    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    // Challenge: Put in onPause() to stop the update

    @Override
    protected void onPause() {
        super.onPause();
        if (fusedLocationClient != null) {
            fusedLocationClient.removeLocationUpdates(myLocationCallBack);
        }
    }
}
