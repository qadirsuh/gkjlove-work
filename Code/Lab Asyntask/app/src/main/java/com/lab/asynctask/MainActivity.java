package com.lab.asynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    // Step 3.1: Define a variable as follows::
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Step 3.2: Append the statement in onCreate()
        textView = (TextView) findViewById(R.id.textView);

    }

    // Step 3.3: Define a method as follows:
    public void butClick(View view) {
        /*int counter = 0;
        while (counter <= 50){
            try {
                Thread.sleep(2000);
                counter++;
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        textView.setText("Button Clicked");*/

        // Step 8: Replace all statements in method, butClick, by the following one statement:
        AsyncTask task = new MyTask().execute();


    }

    //  Step 6: Subclass AsyncTask
    //  Step 6.1: Type the following code inside java file:

    // Step 6.2: Wait a while and you will see some error message
    private class MyTask extends AsyncTask<Void, Integer, String> {

        // Step 6.4: “Implement methods” to populate method template

        // Step 7: Prepare methods in MyTask class
        @Override
        protected void onProgressUpdate(Integer... values) {
            textView.setText("Counter = " + values[0]);
        }

        // Step 7.2: Define (or modify if exists) onPostExecute() as follows:
        @Override
        protected void onPostExecute(String result) {
            textView.setText(result);
        }


        // Step 7.3: Modify doInBackground() as follows:
        @Override
        protected String doInBackground(Void... params) {
            int counter = 0;
            while (counter <= 30) {
                try {
                    Thread.sleep(1000);
                    publishProgress(counter);
                    counter++;
                } catch (Exception e) {
                    return (e.getLocalizedMessage());
                }
            }
            return "Button Clicked";
        }
    }
}
